let displayEntry = $('.calculator-display');
let calcBtn = $('.btn');
let result = 0;
let firstNum = '0';
let secondNum = '0';
let operator = null;
displayEntry.val(0);

function clean() {
	displayEntry.val('0');
	firstNum = '0';
	secondNum = '0';
	operator = null;
}
function roundToSeven(num) {
	return +(Math.round(num + 'e+7') + 'e-7');
}

calcBtn.on('click', function () {
	let pressedBtn = $(this).val();
	displayEntry.val(pressedBtn);
	displayEntry.attr('placeholder', pressedBtn);
	if (isNumber(pressedBtn)) {
		if (operator === null) {
			firstNum === '0' ? (firstNum = pressedBtn) : (firstNum += pressedBtn);
			displayEntry.val(firstNum);
		} else {
			secondNum === '0' ? (secondNum = pressedBtn) : (secondNum += pressedBtn);
			displayEntry.val(secondNum);
		}
	}
	if (isOperator(pressedBtn)) {
		operator = pressedBtn;
		displayEntry.val(operator);
	} else if (pressedBtn === '=') {
		if (firstNum === null || secondNum === null || operator === null) {
			clean();
		} else if (secondNum === '0' && operator === '/') {
			clean();
			displayEntry.val('ERROR');
		} else {
			result = calc(firstNum, secondNum, operator);
			showResult();
			displayEntry.val(result);
			operator = null;
			firstNum = '0';
			secondNum = '0';
		}
	}
});

function isNumber(value) {
	return !isNaN(value);
}

function isOperator(value) {
	return value === '/' || value === 'x' || value === '+' || value === '-';
}

function calc(a, b, operator) {
	let result;
	a = parseFloat(a);
	b = parseFloat(b);
	if (isNaN(a) || isNaN(b)) {
		a = 0;
		b = 0;
	}
	if (operator === '+') {
		return a + b;
	}
	if (operator === '-') {
		return a - b;
	}
	if (operator === 'x') {
		return a * b;
	}
	if (operator === '/') {
		return roundToSeven(a / b);
	}
}

let logsList = $('.logs-list');
let logBlock = $('.logs');

function showResult() {
	let newListItem = document.createElement('li');
	newListItem.setAttribute('class', 'logs-item');
	if (
		/[4][8]/gmy.test(firstNum) ||
		/[4][8]/gmy.test(secondNum) ||
		/[4][8]/gmy.test(result)
	) {
		console.log(firstNum);
		newListItem.innerHTML = `
    <span class="logs-result underlined">
    ${firstNum} ${operator} ${secondNum} = ${result}
    </span>`;
	} else {
		newListItem.innerHTML = `
    <span class="logs-result">
    ${firstNum} ${operator} ${secondNum} = ${result}
    </span>`;
	}

	let newToggleSpan = document.createElement('span');
	newToggleSpan.setAttribute('class', 'logs-flag');
	newListItem.prepend(newToggleSpan);
	newToggleSpan.setAttribute('onclick', 'toggleElement(this)');

	let newRemoveSpan = document.createElement('span');
	newRemoveSpan.setAttribute('class', 'logs-remove');
	newRemoveSpan.setAttribute('onclick', 'remove(this)');
	newRemoveSpan.innerHTML = '&#x2715;';
	newListItem.append(newRemoveSpan);

	logsList.prepend(newListItem);
}

function remove(el) {
	$(el).parent().remove();
}

function toggleElement(el) {
	el.classList.toggle('active');
}

logBlock.on('scroll', function () {
	console.log('Scroll Top: ' + logBlock.scrollTop());
});
